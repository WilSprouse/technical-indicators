/*
 * Indicators.h
 *
 * Purpose: To provide a C++ header that has pre-implemented technical indicators for stock trading
 *
 */
#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std; 

class Indicators {
    
    float mean_dev = 0.0;
    int periods;
    
  public:
    float bollinger_bands[3]; float price_channel_bands[2]; float ma_envs[2]; float aroons[2]; float kelt_bands[2];
    float typical_price_sma, std_dev;
    float sma, cci = 0.0;
    float *prices, *type_prices;//, hlc[20][3];//
    void meanDeviation(float[], float); void channelCommodityIndex(float[][3]); float simpleMovingAverage(float[]); float simpleMovingAverage(float[], int);
    void standardDeviation(); float averageMovingAverage(float[][3]); float averageTrueRange(float[][3], int); void bollingerBands(); void priceChannel(float[][3]);
    void movingAverageEnvelopes(); void aroon(float[][3]); float aroonOscillator(); void keltnerBands(float, float, float);
    void setPeriods(int num_periods)
    {
        periods = num_periods;
    }

    void setClosePrices(float *close_prices)
    {
        prices = close_prices;
    }

    void testPeriods() { //cout << prices[19] << endl; 
    }
};

// Helper Functions for the technical indicators
void
Indicators::meanDeviation(float *typ_prices, float typ_sma)
{
    
    for (int i = 0; i < periods; i++)
    {
        mean_dev += abs(typ_prices[i] - typ_sma);
    }

    mean_dev /= periods;
}

/*  Non-Class version
float
meanDeviation1(float *prices1, float typ_sma1, int periods)
{
    float mean_dev1 = 0.0;
    for (int i = 0; i < periods; i++)
    {
        mean_dev1 += abs(prices1[i] - typ_sma1);
    }

    mean_dev1 /= 20.0;
    return mean_dev1;
}*/


/*float
standardDeviation(float sma, float *prices, int periods)
{
    float std_dev = 0.0;
    for (int i = 0; i < periods; i++)
    {
        std_dev += pow((prices[i]-sma), 2);
    }
    return sqrt(std_dev/periods);

}*/

void
Indicators::standardDeviation()
{
    std_dev = 0.0;
    for (int i = 0; i < periods; i++)
    {
        std_dev += pow((prices[i]-sma), 2);
    }
    std_dev = sqrt(std_dev/periods);

}



// SMA is a helper function for a number of tech indicators, but is also a readily used technical indicator in itself
/*float
simpleMovingAverage(float* prices, int periods)
{
    float sum = 0;
    for (int i = 0; i < periods; i++)
    {
        sum += prices[i];
    }

    return sum/periods;

}*/

float
Indicators::simpleMovingAverage(float *sma_prices)
{
    float sum = 0;
    for (int i = 0; i < periods; i++)
    {
        sum += sma_prices[i];
    }

    return sum/periods;

}

float
Indicators::simpleMovingAverage(float *sma_prices, int cust_periods)
{
    float sum = 0;
    for (int i = 0; i < cust_periods; i++)
    {
        sum += sma_prices[i];
    }

    return sum/cust_periods;

}


/*float
averageMovingAverage(float prices[][3], int periods)
{

    float sum = 0;
    for (int i = 0; i < periods; i++) {
        sum += ((prices[i][0] + prices[i][1] + prices[i][2])/3);
    }
    return sum/periods;

}*/

float
Indicators::averageMovingAverage(float prices[][3])
{

    float sum = 0;
    for (int i = 0; i < periods; i++) {
        sum += ((prices[i][0] + prices[i][1] + prices[i][2])/3);
    }
    return sum/periods;

}


/*float
averageTrueRange(float prices[][3], int price_periods, int atr_periods)
{
    float tr[atr_periods];
    float h, l, c, hl, hc, lp;
    int curr_day;
    for (int i = 0; i < atr_periods; i++)
    {
	h = prices[((price_periods-1)-i)][0];
	l = prices[((price_periods-1)-i)][1];
	c = prices[((price_periods-1)-i)][2];

	hl = h - l;
	hc = abs(h - c);
	lp = abs(l - c);
        
        tr[i] = hl > hc ? (hl > lp ? hl: lp): (hc > lp? hc: lp);
    }
    return simpleMovingAverage(tr, atr_periods);
}*/



float
Indicators::averageTrueRange(float prices[][3], int atr_periods)
{
    float tr[atr_periods];
    float h, l, c, hl, hc, lp;
    int curr_day;
    for (int i = 0; i < atr_periods; i++)
    {
	h = prices[((periods-1)-i)][0];
	l = prices[((periods-1)-i)][1];
	c = prices[((periods-1)-i)][2];

	hl = h - l;
	hc = abs(h - c);
	lp = abs(l - c);
        
        tr[i] = hl > hc ? (hl > lp ? hl: lp): (hc > lp? hc: lp);
    }
    return simpleMovingAverage(tr, atr_periods);
}



void
Indicators::channelCommodityIndex(float typ_price[][3])
{
    // CCI = (Typical Price  -  20-period SMA of TP) / (.015 x Mean Deviation)

    float typ_prices[periods];

    for (int i = 0; i < periods; i++)
    {
        typ_prices[i] = ((typ_price[i][0]+typ_price[i][1]+typ_price[i][2])/3.0);
	//calc_typ_price += typ_prices[i];
    }

    float calc_typ_price = ((typ_price[periods-1][0]+typ_price[periods-1][1]+typ_price[periods-1][2])/3.0);

    typical_price_sma = simpleMovingAverage(typ_prices);
    meanDeviation(typ_prices, typical_price_sma);

    cci = (calc_typ_price - typical_price_sma) / (0.015 * mean_dev);
}

/*float
channelCommodityIndex(float* prices, float typ_price[][3], int periods)
{
    // CCI = (Typical Price  -  20-period SMA of TP) / (.015 x Mean Deviation)

    float typ_prices[periods];

    for (int i = 0; i < periods; i++)
    {
        typ_prices[i] = ((typ_price[i][0]+typ_price[i][1]+typ_price[i][2])/3);
	//calc_typ_price += typ_prices[i];
    }

    float calc_typ_price = ((typ_price[periods-1][0]+typ_price[periods-1][1]+typ_price[periods-1][2])/3.0);

    float calc_typ_sma = simpleMovingAverage(typ_prices, periods);
    float mean_deviation = meanDeviation1(typ_prices, calc_typ_sma, periods);

    return (calc_typ_price - calc_typ_sma) / (0.015 * mean_deviation);
}*/

/*void
bollingerBands(float* prices, int periods, float* update_bands)
{
    float std_dev;

    update_bands[1] = simpleMovingAverage(prices, periods); 		// Middle Band
    std_dev = standardDeviation(update_bands[1], prices, periods);

    update_bands[0] = update_bands[1] + (2 * std_dev);			// Upper Band
    update_bands[2] = update_bands[1] - (2 * std_dev);			// Lower Band

}*/

void
Indicators::bollingerBands()
{
    //float std_dev;

    bollinger_bands[1] = simpleMovingAverage(prices); 		// Middle Band
    standardDeviation();

    bollinger_bands[0] = bollinger_bands[1] + (2 * std_dev);			// Upper Band
    bollinger_bands[2] = bollinger_bands[1] - (2 * std_dev);			// Lower Band

}

/*void
movingAverageEnvelopes(float* prices, int periods, float* update_envs, float sma)
{
    update_envs[0] = sma + (sma * .025);
    update_envs[1] = sma - (sma * .025);
}*/

void
Indicators::movingAverageEnvelopes()
{
    ma_envs[0] = sma + (sma * .025);
    ma_envs[1] = sma - (sma * .025);
}

/*float
relativeStrengthIndex(float* prices, int price_periods, int rsi_periods)
{
    
     *	The price period must be twice as much as the rsi periods, to calculate the first day avg_gain and avg_loss.
     *	Get rid of the if statement checking for this if you want quicker execution.
     
    if (price_periods < (2 * rsi_periods)) {
        cout << "rsi periods needs to be <= half of the prices periods" << endl;
	return 0.0;
    }
    float prev_price, curr_price, rs, avg_gain, avg_loss;
    int num_gains, num_losses;

    int first_calcs = 2*rsi_periods;			

    prev_price = prices[(price_periods - first_calcs)]; // Maybe not -1
    for (int i = 1; i < rsi_periods; i++)
    {
	curr_price = prices[(price_periods - first_calcs)+i];

        if (curr_price > prev_price) {
	    avg_gain += (curr_price-prev_price);
	    num_gains++;
	} else {
	    avg_loss += (prev_price-curr_price);
	    num_losses++;
	}

	prev_price = curr_price;
    }
    avg_gain /= num_gains;
    avg_loss /= num_losses;

    num_gains = 0;
    num_losses = 0;

    for (int i = 1; i < rsi_periods; i++)
    {
	curr_price = prices[(price_periods - rsi_periods)+i];

        if (curr_price > prev_price) {
	    avg_gain = ((avg_gain * (rsi_periods-1) + (curr_price-prev_price)));
	    num_gains++;
	} else {
	    avg_loss += (prev_price-curr_price);
	    avg_loss = ((avg_gain * (rsi_periods-1) + (prev_price-curr_price)));
	    num_losses++;
	}
	prev_price = curr_price;
    }

    avg_gain /= num_gains;//rsi_periods;
    avg_loss /= num_losses;//rsi_periods;
    
    rs = avg_gain/avg_loss;
    return (100 - (100/(1+rs)));
}*/

/*void
priceChannel(float prices[][3], int price_periods, float* channel_bands)
{

    float high = prices[0][0];
    float low = prices[0][1];
    for (int i = 1; i < price_periods; i++ )
    {
        if (prices[i][0] >= high) {
            high = prices[i][0];
	} else if (prices[i][1] <= low) {
            low = prices[i][1];
	}

    }	    
    channel_bands[0] = low;
    channel_bands[1] = high;
}*/

void
Indicators::priceChannel(float prices[][3])
{

    float high = prices[0][0];
    float low = prices[0][1];
    for (int i = 1; i < periods; i++ )
    {
        if (prices[i][0] >= high) {
            high = prices[i][0];
	} else if (prices[i][1] <= low) {
            low = prices[i][1];
	}

    }	    
    price_channel_bands[0] = low;
    price_channel_bands[1] = high;
}

/*void
aroon(float prices[][3], int price_periods, int* aroon_nums)
{

    float high = prices[0][0];
    float low = prices[0][1];
    int days_since_high = 0;
    int days_since_low = 0;
    for (int i = 1; i < price_periods; i++ )
    {
	days_since_high++;
	days_since_low++;
        if (prices[i][0] >= high) {
            high = prices[i][0];
	    days_since_high = 0;
        } else if (prices[i][1] <= low) {
            low = prices[i][1];
	    days_since_low = 0;
        }
    }

    aroon_nums[0] = ((price_periods - days_since_high)*100)/price_periods;
    aroon_nums[1] = ((price_periods - days_since_low)*100)/price_periods;

}*/

void
Indicators::aroon(float prices[][3])
{

    float high = prices[0][0];
    float low = prices[0][1];
    int days_since_high = 0;
    int days_since_low = 0;
    for (int i = 1; i < periods; i++ )
    {
	days_since_high++;
	days_since_low++;
        if (prices[i][0] >= high) {
            high = prices[i][0];
	    days_since_high = 0;
        } else if (prices[i][1] <= low) {
            low = prices[i][1];
	    days_since_low = 0;
        }
    }

    aroons[0] = ((periods - days_since_high)*100)/periods;
    aroons[1] = ((periods - days_since_low)*100)/periods;

}

/*int
aroonOscillator(int* aroon_up_down)
{

    return aroon_up_down[1] - aroon_up_down[0];

}*/

float
Indicators::aroonOscillator()
{

    return aroons[1] - aroons[0];

}


/*void
keltnerBands(float ma, float atr, float y_factor, float* kelt_bands)
{

    kelt_bands[0] = ma - (y_factor * atr);
    kelt_bands[1] = ma + (y_factor * atr);

}*/

void
Indicators::keltnerBands(float ma, float atr, float y_factor)
{
    
    kelt_bands[0] = ma - (y_factor * atr);
    kelt_bands[1] = ma + (y_factor * atr);

}


int main() {

    Indicators ind;
    ind.setPeriods(20);



    int periods = 20;
    
    float prices[periods] = {
        16.50, 16.89, 16.72, 16.01, 16.01, 
	14.97, 14.56, 14.62, 13.53, 13.4, 
	13.06, 12.75, 12.28, 12.71, 13.71, 
	14.2, 13.32, 12.45, 12.94, 12.98	    
    };

    ind.setClosePrices(prices);
    

    ind.testPeriods();

    float hlc[periods][3] = {
	    {16.57, 15.35, 16.50}, {17.25, 16.32, 16.89}, {17.46, 16.4, 16.72}, {16.82, 15.94, 16.01}, {16.2485, 15.94, 16.01},
	    {15.75, 14.9201, 14.97}, {15.25, 14.54, 14.56}, {15.5499, 14.57, 14.62}, {14.478, 13.50, 13.53}, {13.41, 11.75, 13.53},
	    {13.60, 12.83, 13.06}, {13.82, 12.68, 12.75}, {13.06, 12.2, 12.28}, {12.88, 11.89, 12.71}, {13.74, 12.76, 13.71},
	    {14.37, 13.56, 14.2}, {14.2, 13.19, 13.32}, {13.32, 12.42, 12.45}, {13.09, 12.265, 12.94}, {13.6, 12.94, 12.98}

    };

    //float SMA = simpleMovingAverage(prices, periods);
    ind.sma = ind.simpleMovingAverage(ind.prices);
    ind.standardDeviation();
    cout << "SMA (Class): " << ind.sma  << endl;
    cout << "StdDev (Class): " << ind.std_dev  << endl;
    //cout << standardDeviation(SMA, prices, 20) << endl;
    //float AVA = averageMovingAverage(hlc, periods);
    float AVA_class = ind.averageMovingAverage(hlc);
    //cout << "AVA: " << AVA << "AVA (Class): " << AVA_class  << endl;
    //float CCI = channelCommodityIndex(prices, hlc, periods);
    //ind.channelCommodityIndex(hlc);
    ind.channelCommodityIndex(hlc);
    cout << "CCI (Class): "  << ind.cci << endl;
    //cout << "CCI: " << CCI << endl;
    //float ATR = averageTrueRange(hlc, 20, 14);
    //float RSI = relativeStrengthIndex(prices, periods, 5);

    // Bollinger Bands Updates a malloced array with the bands
    //float *bands = (float*) malloc(sizeof(float)*3);    
    //bollingerBands(prices, periods, bands);
    ind.bollingerBands();

    // Moving Average Envelopes updates a malloced array with the envelopes
    //float *envelopes = (float*) malloc(sizeof(float)*2);
    //movingAverageEnvelopes(prices, periods, envelopes, SMA);
    ind.movingAverageEnvelopes();

    // Price Channel updates a malloced array (price_channel) with the bands
    //float *price_channel = (float*) malloc(sizeof(float)*2);
    //priceChannel(hlc, periods, price_channel);
    ind.priceChannel(hlc);

    //int *aroon_high_low = (int*) malloc(sizeof(int)*2);
    //aroon(hlc, periods, aroon_high_low);
    ind.aroon(hlc);

    //float *keltner_bands = (float*) malloc(sizeof(float)*2);
    //keltnerBands(AVA, ATR, 1, keltner_bands);
    float atr_class = ind.averageTrueRange(hlc, 14);
    ind.keltnerBands(AVA_class, atr_class, 1);


    //cout << "SMA: " << SMA << endl;
    //cout << "BB-Upper " << bands[0] << endl;
    //cout << "BB-Middle " << bands[1] << endl;
    //cout << "BB-lower " << bands[2] << endl;
    cout << "BB-Upper (Class): " << ind.bollinger_bands[0] << endl;
    cout << "BB-Middle (Class): " << ind.bollinger_bands[1] << endl;
    cout << "BB-lower (Class): " << ind.bollinger_bands[2] << endl;
    //cout << "Upper-env: " << envelopes[0] << endl;
    //cout << "Lower-env: " << envelopes[1] << endl;
    cout << "Upper-env (Class): " << ind.ma_envs[0] << endl;
    cout << "Lower-env (Class): " << ind.ma_envs[1] << endl;
    //cout << "ATR: " << ATR << endl;
    cout << "ATR (Class): " << ind.averageTrueRange(hlc, 14) << endl;
    //cout << "RSI (Incorrect): " << RSI << endl;
    //cout << "Channel Bands: " << price_channel[0] << " " << price_channel[1] << endl;
    cout << "Channel Bands (Class): " << ind.price_channel_bands[0] << " " << ind.price_channel_bands[1] << endl;
    //cout << "Aroon: " << aroon_high_low[0] << " " << aroon_high_low[1] << endl;
    cout << "Aroon (Class): " << ind.aroons[0] << " " << ind.aroons[1] << endl;
    //cout << "Aroon Oscillator: " << aroonOscillator(aroon_high_low)<< endl;
    cout << "Aroon Oscillator (Class): " << ind.aroonOscillator()<< endl;
    //cout << "Keltner Bands: " << keltner_bands[0] << " " << keltner_bands[1] << endl;
    cout << "Keltner Bands (Class): " << ind.kelt_bands[0] << " " << ind.kelt_bands[1] << endl;

    /*free(bands);
    free(envelopes);
    free(price_channel);
    free(aroon_high_low);
    free(keltner_bands);*/

    return 0;
}
